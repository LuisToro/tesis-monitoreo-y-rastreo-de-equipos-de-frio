'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Schedules', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      minuto: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      hora: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      dia: {
        type: Sequelize.STRING,
        allowNull: false
      },
      ClientId: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Schedules');
  }
};