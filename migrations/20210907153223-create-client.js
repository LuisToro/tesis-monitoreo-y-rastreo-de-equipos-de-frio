'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Clients', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombre: {
        type: Sequelize.STRING
      },
      numero: {
        type: Sequelize.INTEGER
      },
      codigoContrato: {
        type: Sequelize.STRING
      },
      direccion: {
        type: Sequelize.STRING
      },
      detalle: {
        type: Sequelize.STRING
      },
      equipoFrio: {
        type: Sequelize.STRING
      },
      descripcionEquipoFrio: {
        type: Sequelize.STRING
      },
      celularEquipoFrio: {
        type: Sequelize.STRING
      },
      CompanyId: {
        type: Sequelize.INTEGER,
        allowNull: false
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Clients');
  }
};