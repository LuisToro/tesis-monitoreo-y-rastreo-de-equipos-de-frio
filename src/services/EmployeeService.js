const database = require('../models/employee');

const EmployeeService = {

  getAllEmployees: async () => {
    try {
      return await database.findAll();
    } catch (error) {
      throw error;
    }
  },

  
  getEmployeeByEmail: async (email) => {
    try {
      const companyId = await database.findAll({
        where: { email: String(email) }
      });
      return companyId[0];
    } catch (error) {
      throw error;
    }
  },

  getCompanyIdByEmail: async (email) => {
    try {
      const companyId = await database.findAll({
        where: { email: String(email) },
        attributes:['CompanyId'],
      });
      return companyId[0];
    } catch (error) {
      throw error;
    }
  },

  getPasswordByEmail: async (email) => {
    try {
      const companyId = await database.findAll({
        where: { email: String(email) },
        attributes:['password'],
      });
      return companyId[0];
    } catch (error) {
      throw error;
    }
  },

  getAllEmployeesByCompany: async (companyId) => {
    let id = 0;
    if (isNaN(companyId)) {
      id = companyId.CompanyId;
    } else {
      id = companyId;
    }
    try {
      const employeesByCompany = await database.findAll({
        where: { CompanyId: Number(id) },
        attributes: { exclude: 'CompanyId' },
      });
      return employeesByCompany;
    } catch (error) {
      throw error;
    }
  },

  createEmployee: async (newEmployee) => {
    try {
      return await database.create(newEmployee);
    } catch (error) {
      throw error;
    }
  },

  updateEmployee: async (id, updateEmployee) => {
    try {
      const employeeToUpdate = await database.findOne({
        where: { id: Number(id) }
      });

      if (employeeToUpdate) {
        await database.update(updateEmployee, { where: { id: Number(id) } });

        return updateEmployee;
      }
      return null;
    } catch (error) {
      throw error;
    }
  },

  getAEmployee: async (id) => {
    try {
      const employee = await database.findOne({
        where: { id: Number(id) }
      });

      return employee;
    } catch (error) {
      throw error;
    }
  },

  deleteEmployee: async (idEmployee, companyId) => {
    try {
      const employeeToDelete = await database.findOne({ where: { id: Number(idEmployee), CompanyId: Number(companyId) } });
      if (employeeToDelete) {
        const deletedEmployee = await database.destroy({
          where: { id: Number(idEmployee), CompanyId: Number(companyId) }
        });
        return deletedEmployee;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = EmployeeService;