const database = require('../models/employee');
var ManagementClient = require('auth0').ManagementClient;

var auth0 = new ManagementClient({
    domain: process.env.AUTH0_DOMAIN,
    clientId: process.env.AUTH0_CLIENT_ID,
    clientSecret: process.env.AUTH0_CLIENT_SECRET
});

const Auth0Service = {

    getAuthUserByEmail: async (email) => {
        try {
            return auth0.getUsersByEmail(email).then(function (user) {
                return user[0].user_id

            }).catch(function (error) {
                console.log(error);
            });
        } catch (error) {
            console.log(error);
            throw (error)
        }

    },
    createEmployeeInAuth: async (newEmployee) => {
        try {
            return await auth0.createUser(newEmployee)
        } catch (error) {
            throw error;
        }
    },
    updateEmployee: async (id, data) => {
        try {
            return await auth0.updateUser({ id: String(id) }, data);
        } catch (error) {
            throw error;
        }
    },
    deleteEmployeeInAuth: async (id) => {
        try {
            return await auth0.deleteUser({ id: id })
        } catch (error) {
            throw error;
        }
    },
}
module.exports = Auth0Service;
