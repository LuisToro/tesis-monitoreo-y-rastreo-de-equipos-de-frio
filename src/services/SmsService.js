require('dotenv').config();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

const axios = require('axios');
const SmsService = {

    sendSms: async (cellPhone) => {
        try {
            return await client.messages.create(
                {
                    body: 'WHERE,666666#',
                    from: '+13512223211',
                    to: cellPhone
                }

            )
        } catch (error) {
            throw error;
        }
    },

    getLastSms: async (cellPhone, year, month, day, hour, minute, second) => {
        try {
            const lastMessage = await client.messages.list(
                {
                    from: cellPhone,
                    dateSentAfter: new Date(Date.UTC(year, month, day, hour, minute, second))
                }
            )
            return lastMessage[0].body
        } catch (error) {
            throw error;
        }
    },

    getLatitude: async (message) => {
        const regex = /:/g;
        const splitMessage = message.split("\n", 2);
        const latitude = regex[Symbol.split](splitMessage[1]);

        return latitude[1].split("", 1) === "N" || latitude[1].split("", 1) === "S" ?
            latitude[1].substring(1) : latitude[1].replace(latitude[1].split("", 1), "-");
    },

    getLength: async (message) => {
        const regex = /:/g;
        const splitMessage = message.split("\n", 3);
        const length = regex[Symbol.split](splitMessage[2]);

        const result = (length[1].split("", 1) === "E" || length[1].split("", 1) === "O" ?
            length[1].substring(1) : length[1].replace(length[1].split("", 1), "-")).slice(0, -5);
        return result
    },

    getLastTwenty: async (cellPhone) => {
        try {
            const lastMessages = await client.messages.list(
                {
                    from: cellPhone,
                    limit: 20
                }
            )
            return lastMessages
        } catch (error) {
            throw error;
        }
    },

    reverseGeo: async (lat, lon) => {
        try {
            return axios.get(`https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lon}`)
                .then(response => {
                    return response.data.address
                })
                .catch(error => {
                    console.log(error);
                });

        } catch (error) {
            console.log(error);
            throw (error)
        }

    }
}
module.exports = SmsService;