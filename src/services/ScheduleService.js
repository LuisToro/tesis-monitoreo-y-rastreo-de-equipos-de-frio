require('dotenv').config();
const database = require('../models/schedule');
const cron = require('node-cron');
const SmsService = require('./SmsService')
const ClientService = require('./ClientService')

const codigo = process.env.CODIGO_BOLIVIA;
const ScheduleService = {
  getAllSchedules: async () => {
    try {
      return await database.findAll();
    } catch (error) {
      throw error;
    }
  },

  getAllShcedulesByClient: async (clientId) => {
    try {
      const schedulesByClient = await database.findAll({
        where: { ClientId: Number(clientId) },
        attributes: { exclude: 'ClientId' }
      });
      return schedulesByClient;
    } catch (error) {
      throw error;
    }
  },

  createSchedule: async (newSchedule) => {
    try {
      return await database.create(newSchedule);
    } catch (error) {
      throw error;
    }
  },

  getMinute: async (ClientId) => {
    try {
      const minute = await database.findOne({
        where: { ClientId: Number(ClientId) },
        attributes: ['minuto']
      });
      return minute;
    } catch (error) {
      throw error;
    }
  },
  getHour: async (ClientId) => {
    try {
      const hour = await database.findOne({
        where: { ClientId: Number(ClientId) },
        attributes: ['hora']
      });
      return hour;
    } catch (error) {
      throw error;
    }
  },
  getDay: async (ClientId) => {
    try {
      const day = await database.findOne({
        where: { ClientId: Number(ClientId) },
        attributes: ['dia']
      });
      return day;
    } catch (error) {
      throw error;
    }
  },

  verifyIfExistsJobForToday: async () => {
    var sortedByHour
    var today = new Intl.DateTimeFormat('en-US', { weekday: 'long' }).format(new Date());
    var schedules = Array(0);
    var tasks = await ScheduleService.getAllSchedules();

    for (let index = 0; index < tasks.length; index++) {
      if (tasks[index]['dia'].toUpperCase() == today.toUpperCase()) {
        schedules.push(tasks[index]);
      }
    }
    sortedByHour = schedules.sort((firstItem, secondItem) => firstItem.hora - secondItem.hora)
    return sortedByHour.sort((firstItem, secondItem) => firstItem.minute - secondItem.minute)
  },

  cronOfCrons: () => {
    var task
    var listOftask
    try {
      task = cron.schedule('10 10 3 * * *', async function () {
        listOftask = await ScheduleService.verifyIfExistsJobForToday();
        if (listOftask.length > 0) {
          listOftask.forEach(listOftasks => {
            ScheduleService.runSchedules(listOftasks.dia, listOftasks.minuto, listOftasks.hora, listOftasks.ClientId)
          });
        }
      }, {
        timezone: "America/La_Paz"
      });
    } catch (error) {
      throw error;
    }
  },

  runSchedules: async (day, minute, hour, ClientId) => {
    var task
    var clientCellPhone = await ClientService.getAClient(ClientId)
    try {
      task = cron.schedule(`59 ${minute} ${hour} * * ${day}`, () => {
        SmsService.sendSms(`${codigo}${clientCellPhone.celularEquipoFrio}`);
      }, {
        timezone: "America/La_Paz"
      });
    } catch (error) {
      throw error;
    }
  },

  deleteSchedule: async (idSchedule, clientId) => {
    try {
      const scheduleToDelete = await database.findOne({ where: { id: Number(idSchedule), ClientId: Number(clientId) } });
      if (scheduleToDelete) {
        const deletedSchedule = await database.destroy({
          where: { id: Number(idSchedule), ClientId: Number(clientId) }
        });
        return deletedSchedule;
      }
      return null;
    } catch (error) {
      throw error;
    }
  },

  updateSchedule: async (id, updateSchedule) => {
    try {
      const scheduleToUpdate = await database.findOne({
        where: { id: Number(id) }
      });

      if (scheduleToUpdate) {
        await database.update(updateSchedule, { where: { id: Number(id) } });

        return updateSchedule;
      }
      return null;
    } catch (error) {
      throw error;
    }
  },
}

module.exports = ScheduleService;