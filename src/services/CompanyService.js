const database = require('../models/company');
const EmployeeService = require('./EmployeeService')
const ClientService = require('./ClientService')

const CompanyService = {

  getAllCompanies: async () => {
    try {
      return await database.findAll();
    } catch (error) {
      throw error;
    }
  },

  createCompany: async (newCompany) => {
    try {
      return await database.create(newCompany);
    } catch (error) {
      throw error;
    }
  },

  updateCompany: async (id, updateCompany) => {
    try {
      const companyToUpdate = await database.findOne({
        where: { id: Number(id) }
      });

      if (companyToUpdate) {
        await database.update(updateCompany, { where: { id: Number(id) } });

        return updateCompany;
      }
      return null;
    } catch (error) {
      throw error;
    }
  },

  getACompany: async (id) => {
    try {
      const company = await database.findOne({
        where: { id: Number(id) }
      });

      return company;
    } catch (error) {
      throw error;
    }
  },

  deleteCompany: async (id) => {
    try {
      const companyToDelete = await database.findOne({ where: { id: Number(id) } });
      clients = await ClientService.getAllToDelete({ CompanyId: Number(id) })
      employees = await EmployeeService.getAllEmployeesByCompany({ CompanyId: Number(id) })
      if (companyToDelete) {
        if (clients.length > 0) {
          clients.forEach(client => {
            ClientService.deleteClient(client.id, id)
          })
        }
        if (employees.length > 0) {
          employees.forEach(employee => {
            EmployeeService.deleteEmployee(employee.id, id)
          })
        }
        const deletedCompany = await database.destroy({
          where: { id: Number(id) }
        });
        return deletedCompany;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = CompanyService;