const database = require('../models/client');
const EmployeeService =require('./EmployeeService');
const ClientService = {

  getAllClients: async () => {
    try {
      return await database.findAll();
    } catch (error) {
      throw error;
    }
  },

  getAllToDelete: async (companyId) => {
    let id = 0;
    if (isNaN(companyId)) {
      id = companyId.CompanyId;
    } else {
      id = companyId;
    }
    try {
      const clientsToDelete = await database.findAll({
        where: { CompanyId: Number(id) },
      });
      return clientsToDelete;
    } catch (error) {
      throw error;
    }
  },

  getAllClientsByCompany: async (email) => {
    try {
      const id = await EmployeeService.getCompanyIdByEmail(email)
      const clientsByCompany = await database.findAll({
        where: { CompanyId: Number(id.CompanyId) },
        attributes: { exclude: 'CompanyId' }
      });
      return clientsByCompany;
    } catch (error) {
      throw error;
    }
  },

  createClient: async (newClient) => {
    try {
      return await database.create(newClient);
    } catch (error) {
      throw error;
    }
  },

  updateClient: async (id, updateClient) => {
    try {
      const clientToUpdate = await database.findOne({
        where: { id: Number(id) }
      });

      if (clientToUpdate) {
        await database.update(updateClient, { where: { id: Number(id) } });

        return updateClient;
      }
      return null;
    } catch (error) {
      throw error;
    }
  },

  getAClient: async (id) => {
    try {
      const client = await database.findOne({
        where: { id: Number(id) }
      });

      return client;
    } catch (error) {
      throw error;
    }
  },

  deleteClient: async (idClient) => {
    try {
      const clientToDelete = await database.findOne({ where: { id: Number(idClient) } });

      if (clientToDelete) {
        const deletedClient = await database.destroy({
          where: { id: Number(idClient) }
        });
        return deletedClient;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = ClientService;