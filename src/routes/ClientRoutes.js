const ClientController = require ('../controllers/ClientController');
const { Router } = require("express");
const router = Router();

router.get('/clients', ClientController.getAllClients);
router.post('/clients', ClientController.createClient);
router.get('/clients/:id', ClientController.getAClient);
router.get('/clientss/:email', ClientController.getAllClientsByCompany);
router.put('/clients/:id', ClientController.updateClient);
router.delete('/clients/:id', ClientController.deleteClient);

module.exports = router;