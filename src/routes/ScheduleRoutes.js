const ScheduleController = require ('../controllers/ScheduleController');
const { Router } = require("express");
const router = Router();

router.get('/schedule', ScheduleController.getAllSchedules);
router.get('/schedule/:id', ScheduleController.getAllScheduleByClient);
router.get('/schedule/minute/:id', ScheduleController.getMinute);
router.get('/schedule/hour/:id', ScheduleController.getHour);
router.get('/schedule/day/:id', ScheduleController.getDay);
router.post('/schedule', ScheduleController.createSchedule);
router.post('/schedules', ScheduleController.cronOfCrons);
router.delete('/schedule/:id/client/:clientId', ScheduleController.deleteSchedule);
router.put('/schedule/:id', ScheduleController.updateSchedule);

module.exports = router;