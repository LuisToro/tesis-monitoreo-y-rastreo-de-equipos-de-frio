const EmployeeController = require ('../controllers/EmployeeController');
const { Router } = require("express");
const router = Router();

router.post('/auth', EmployeeController.createEmployeeInAuth);
router.delete('/auth/:email', EmployeeController.deleteEmployeeInAuth);
router.get('/auth/:email', EmployeeController.getAuthUserByEmail);
router.put('/auth0/:email', EmployeeController.updateAuth0Employee);

module.exports = router;