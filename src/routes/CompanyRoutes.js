const CompanyController = require ('../controllers/CompanyController');
const { Router } = require("express");
const router = Router();

router.get('/company', CompanyController.getAllCompanies);
router.post('/company', CompanyController.createCompany);
router.get('/company/:id', CompanyController.getACompany);
router.put('/company/:id', CompanyController.updateCompany);
router.delete('/company/:id', CompanyController.deleteCompany);

module.exports = router;