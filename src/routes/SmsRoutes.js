const SmsController = require ('../controllers/SmsController');
const { Router } = require("express");
const router = Router();

router.post('/sms/:cellPhone', SmsController.sendSms);
router.get('/sms/:cellPhone/:year/:month/:day/:hour/:minute/:second', SmsController.getLastSms);
router.get('/sms/:cellPhone', SmsController.getLastTwenty);
router.get('/smss/:lat/:lon', SmsController.reverseGeo);
module.exports = router;