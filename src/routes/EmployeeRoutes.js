const EmployeeController = require ('../controllers/EmployeeController');
const { Router } = require("express");
const router = Router();

router.get('/employees', EmployeeController.getAllEmployees);
router.get('/employeess/:email', EmployeeController.getCompanyIdByEmail);
router.get('/employees/:email', EmployeeController.getPasswordByEmail);
router.post('/employees', EmployeeController.createEmployee);
router.get('/employees/:id', EmployeeController.getAEmployee);
router.get('/employeesss/:id', EmployeeController.getAllEmployeesByCompany);
router.put('/employees/:id', EmployeeController.updateEmployee);
router.delete('/employees/:id/email/:email/company/:companyId', EmployeeController.deleteEmployee);

module.exports = router;