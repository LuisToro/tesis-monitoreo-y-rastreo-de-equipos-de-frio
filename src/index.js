const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const port = process.env.PORT || 8000;
const cors = require("cors");
const ScheduleController = require("./controllers/ScheduleController");

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.urlencoded());

app.use(require("./routes/ClientRoutes"));
app.use(require("./routes/SmsRoutes"));
app.use(require("./routes/EmployeeRoutes"));
app.use(require("./routes/CompanyRoutes"));
app.use(require("./routes/ScheduleRoutes"));
app.use(require("./routes/Auht0Routes"));

app.get("*", function (peticion, respuesta) {
    respuesta.send("Monitoreo y rastreo");
});

app.listen(port, () => {
    console.log(`Tu aplicacion esta corriendo en http://localhost:${port}`);
    
    (async function () {
        ScheduleController.cronOfCrons();
    })();

});