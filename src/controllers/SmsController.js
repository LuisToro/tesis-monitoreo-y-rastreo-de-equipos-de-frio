const SmsService = require('../services/SmsService');
const util = require('../utils/Utils');

const codigo = process.env.CODIGO_BOLIVIA;

const SmsController = {

  sendSms: async (req, res) => {
    const { cellPhone } = req.params;
    try {
      const sms = await SmsService.sendSms(`${codigo}${cellPhone}`);
      util.setSuccess(201, 'Message Sent!', sms);
      return util.send(res);
    }
    catch (error) {
      console.log(error)
      util.setError(400, error.message);
      return util.send(res);
    }
  },

  getLastSms: async (req, res) => {
    const { cellPhone } = req.params;
    const { year } = req.params;
    const { month } = req.params;
    const { day } = req.params;
    const { hour } = req.params;
    const { minute } = req.params;
    const { second } = req.params;

    if (!cellPhone) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res)
    }
    try {
      const sms = await SmsService.getLastSms(`${codigo}${cellPhone}`, year, month, day, hour, minute, second);
      util.setSuccess(200, 'Last message loaded', sms);
      return util.send(res);
    }
    catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  },

  getLastTwenty: async (req, res) => {
    const { cellPhone } = req.params;
    if (!cellPhone) {
      util.setError(400, 'Please input a valid numeric value');
      return util.send(res)
    }
    try {
      const sms = await SmsService.getLastTwenty(`${codigo}${cellPhone}`);

      if (sms.length > 0) {
          for (let index = 0; index < sms.length; index++) {
            const lon = await SmsService.getLength(sms[index].body);
            const lat = await SmsService.getLatitude(sms[index].body);
            sms[index].address = await SmsService.reverseGeo(lat, lon);
          }
        util.setSuccess(200, 'Messages retrieved', sms);
      } else {
        util.setSuccess(200, 'No Messages found');
      }
      return util.send(res);
    }
    catch (error) {
      util.setError(400, error.message);
      return util.send(res);
    }
  },

  reverseGeo: async (req, res) => {
    const { lat } = req.params;
    const { lon } = req.params;
    try {
      const sms = await SmsService.reverseGeo(lat, lon);
      util.setSuccess(201, 'Message Sent!', sms);
      return util.send(res);
    }
    catch (error) {
      console.log(error)
      util.setError(400, error.message);
      return util.send(res);
    }
  }
}

module.exports = SmsController;