const ClientService = require('../services/ClientService');
const util = require('../utils/Utils');

const ClientController = {
    getAllClients: async (req, res) => {
        try {
            const allClients = await ClientService.getAllClients();
            if (allClients.length > 0) {
                util.setSuccess(200, 'Clients retrieved', allClients);
            } else {
                util.setSuccess(200, 'No Clients found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getAllToDelete: async (req, res) => {
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const clientToDelete = await ClientService.getAllToDelete(id);
            if (clientToDelete.length > 0) {
                util.setSuccess(200, 'Employee found', clientToDelete);
            } else {
                util.setError(200, `Cannot find Employee with the Company id ${id}`);
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getAllClientsByCompany: async (req, res) => {
        const { email } = req.params;
        try {
            const clientsByCompany = await ClientService.getAllClientsByCompany(email);
            if (clientsByCompany.length > 0) {
                util.setSuccess(200, 'Clients found',clientsByCompany);
            } else {
                util.setError(200, `Cannot find Clients with the Company id ${email}`);
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    createClient: async (req, res) => {
        if (!req.body.nombre) {
            util.setError(400, 'Please provide complete details');
            return util.send(res);
        }
        const newClient = req.body;
        try {
            const createdClient = await ClientService.createClient(newClient);
            util.setSuccess(201, 'Client Added!', createdClient);
            return util.send(res);
        } catch (error) {
            util.setError(400, error.message);
            return util.send(res);
        }
    },

    updateClient: async (req, res) => {
        const alteredClient = req.body;
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const updateClient = await ClientService.updateClient(id, alteredClient);
            if (!updateClient) {
                util.setError(404, `Cannot find a Client with the id: ${id}`);
            } else {
                util.setSuccess(200, 'Client updated', updateClient);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    },

    getAClient: async (req, res) => {
        const { id } = req.params;

        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }

        try {
            const theClient = await ClientService.getAClient(id);

            if (!theClient) {
                util.setError(404, `Cannot find Client with the id ${id}`);
            } else {
                util.setSuccess(200, 'Found Client', theClient);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    },

    deleteClient: async (req, res) => {
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please provide a numeric value');
            return util.send(res);
        }
        try {
            const clientToDelete = await ClientService.deleteClient(id);

            if (clientToDelete) {
                util.setSuccess(200, 'Client deleted');
            } else {
                util.setError(404, `Client with the id ${id} cannot be found in the company`);
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    }
}

module.exports = ClientController;