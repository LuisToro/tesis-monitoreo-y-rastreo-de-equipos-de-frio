const ScheduleService = require('../services/ScheduleService');
const util = require('../utils/Utils');

const ScheduleController = {
    getAllSchedules: async (req, res) => {
        try {
            const allSchedules = await ScheduleService.getAllSchedules();
            if (allSchedules.length > 0) {
                util.setSuccess(200, 'Schedules retrieved', allSchedules);
            } else {
                util.setSuccess(200, 'No Schedules found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getAllScheduleByClient: async (req, res) => {
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const schedulesByClient = await ScheduleService.getAllShcedulesByClient(id);
            if (schedulesByClient.length > 0) {
                util.setSuccess(200, 'Schedules found',schedulesByClient);
            } else {
                util.setError(200, `Cannot find Schedules with the Client id ${id}`);
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getMinute: async (req, res) => {
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const minute = await ScheduleService.getMinute(id);
            util.setSuccess(200, 'Minute found', minute);
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getHour: async (req, res) => {
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const hour = await ScheduleService.getHour(id);
            util.setSuccess(200, 'Hour found', hour);
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getDay: async (req, res) => {
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const day = await ScheduleService.getDay(id);
            util.setSuccess(200, 'Day found', day);
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    createSchedule: async (req, res) => {
        const newSchedule = req.body;
        try {
            const createdSchedule = await ScheduleService.createSchedule(newSchedule);
            util.setSuccess(201, 'chedule Added!', createdSchedule);
            return util.send(res);
        } catch (error) {
            util.setError(400, error.message);
            return util.send(res);
        }
    },
    cronOfCrons: async (req, res) => {
        try {
            await ScheduleService.cronOfCrons();
        }
        catch (error) {
            console.log(error)
            util.setError(400, error.message);
            return util.send(res);
        }
    },
    deleteSchedule: async (req, res) => {
        const { id } = req.params;
        const { clientId } = req.params

        if (!Number(id)) {
            util.setError(400, 'Please provide a numeric value');
            return util.send(res);
        }

        try {
            const scheduleToDelete = await ScheduleService.deleteSchedule(id, clientId);

            if (scheduleToDelete) {
                util.setSuccess(200, 'Schedule deleted');
            } else {
                util.setError(404, `the client with the id ${clientId} doesnt have a schedule with the id ${id}`);
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },
    updateSchedule: async (req, res) => {
        const alteredSchedule = req.body;
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const updateSchedule = await ScheduleService.updateSchedule(id, alteredSchedule);
            if (!updateSchedule) {
                util.setError(404, `Cannot find a Schedule with the id: ${id}`);
            } else {
                util.setSuccess(200, 'Schedule updated', updateSchedule);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    }
}

module.exports = ScheduleController;