const CompanyService = require('../services/CompanyService');
const util = require('../utils/Utils');

const CompanyController = {
    getAllCompanies: async (req, res) => {
        try {
            const allCompanies = await CompanyService.getAllCompanies();
            if (allCompanies.length > 0) {
                util.setSuccess(200, 'Companies retrieved', allCompanies);
            } else {
                util.setSuccess(200, 'No Companies found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    createCompany: async (req, res) => {
        if (!req.body.nombre) {
            util.setError(400, 'Please provide complete details');
            return util.send(res);
        }
        const newCompany = req.body;
        try {
            const createdCompany = await CompanyService.createCompany(newCompany);
            util.setSuccess(201, 'Company Added!', createdCompany);
            return util.send(res);
        } catch (error) {
            util.setError(400, error.message);
            return util.send(res);
        }
    },

    updateCompany: async (req, res) => {
        const alteredCompany = req.body;
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const updateCompany = await CompanyService.updateCompany(id, alteredCompany);
            if (!updateCompany) {
                util.setError(404, `Cannot find Companies with the id: ${id}`);
            } else {
                util.setSuccess(200, 'Company updated', updateCompany);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    },

    getACompany: async (req, res) => {
        const { id } = req.params;

        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }

        try {
            const theCompany = await CompanyService.getACompany(id);

            if (!theCompany) {
                util.setError(404, `Cannot find Company with the id ${id}`);
            } else {
                util.setSuccess(200, 'Found Company', theCompany);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    },

    deleteCompany: async (req, res) => {
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please provide a numeric value');
            return util.send(res);
        }
        try {
            const companyToDelete = await CompanyService.deleteCompany(id);
            if (companyToDelete) {
                util.setSuccess(200, 'Company deleted');
            } else {
                util.setError(404, `Company with the id ${id} cannot be found`);
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    }
}

module.exports = CompanyController;