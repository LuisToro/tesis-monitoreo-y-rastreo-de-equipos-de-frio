const EmployeeService = require('../services/EmployeeService');
const Auth0Service = require('../services/Auth0Service');
const util = require('../utils/Utils');

const EmployeeController = {
    getAllEmployees: async (req, res) => {
        try {
            const allEmployees = await EmployeeService.getAllEmployees();
            if (allEmployees.length > 0) {
                util.setSuccess(200, 'Employees retrieved', allEmployees);
            } else {
                util.setSuccess(200, 'No Employees found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getEmployeeByEmail: async (req, res) => {
        const { email } = req.params;
        try {
            const companyId = await EmployeeService.getEmployeeByEmail(email);
            if (companyId) {
                util.setSuccess(200, 'Employee found', companyId);
            } else {
                util.setSuccess(404, 'No Company found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getCompanyIdByEmail: async (req, res) => {
        const { email } = req.params;
        try {
            const companyId = await EmployeeService.getCompanyIdByEmail(email);
            if (companyId) {
                util.setSuccess(200, 'Company ID', companyId);
            } else {
                util.setSuccess(404, 'No Company found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getPasswordByEmail: async (req, res) => {
        const { email } = req.params;
        try {
            const employeeId = await EmployeeService.getEmployeeByEmail(email);
            const password = await EmployeeService.getPasswordByEmail(email);
            if (password) {
                util.setSuccess(200, 'Company ID', password);
            } else {
                util.setSuccess(404, 'No Company found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getAllEmployeesByCompany: async (req, res) => {
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const employeesByCompany = await EmployeeService.getAllEmployeesByCompany(id);
            if (employeesByCompany.length > 0) {
                util.setSuccess(200, 'Employee found', employeesByCompany);
            } else {
                util.setError(200, `Cannot find Employee with the Company id ${id}`);
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    createEmployee: async (req, res) => {
        if (!req.body.nombre) {
            util.setError(400, 'Please provide complete details');
            return util.send(res);
        }
        const newEmployee = {"nombre":req.body.nombre,"email":req.body.email,"password":1,"CompanyId":req.body.CompanyId};
        const auth0Employee = { "connection": "Username-Password-Authentication", "email": req.body.email, "password": req.body.password }
        try {
            const createdEmployee = await EmployeeService.createEmployee(newEmployee);
            await Auth0Service.createEmployeeInAuth(auth0Employee)
            util.setSuccess(201, 'Employee Added!', createdEmployee);
            return util.send(res);
        } catch (error) {
            util.setError(400, error.message);
            return util.send(res);
        }
    },

    updateEmployee: async (req, res) => {
        const alteredEmployee = req.body;
        const { id } = req.params;
        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }
        try {
            const updateEmployee = await EmployeeService.updateEmployee(id, alteredEmployee);
            if (!updateEmployee) {
                util.setError(404, `Cannot find Employees with the id: ${id}`);
            } else {
                util.setSuccess(200, 'Employee updated', updateEmployee);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    },

    getAEmployee: async (req, res) => {
        const { id } = req.params;

        if (!Number(id)) {
            util.setError(400, 'Please input a valid numeric value');
            return util.send(res);
        }

        try {
            const theEmployee = await EmployeeService.getAEmployee(id);

            if (!theEmployee) {
                util.setError(404, `Cannot find Employee with the id ${id}`);
            } else {
                util.setSuccess(200, 'Found Employee', theEmployee);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    },

    deleteEmployee: async (req, res) => {
        const { id } = req.params;
        const { email } = req.params
        const { companyId } = req.params
        if (!Number(id)) {
            util.setError(400, 'Please provide a numeric value');
            return util.send(res);
        }
        try {
            const employeeToDelete = await EmployeeService.deleteEmployee(id, companyId);
            const employeeId = await Auth0Service.getAuthUserByEmail(email)
            const authdeleted = await Auth0Service.deleteEmployeeInAuth(employeeId);
            if (employeeToDelete) {
                util.setSuccess(200, 'Employee deleted');
            } else {
                util.setError(404, `Employee with the id ${id} cannot be found in the comapany ${companyId}`);
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    getAuthUserByEmail: async (req, res) => {
        const { email } = req.params;
        try {
            const user = await Auth0Service.getAuthUserByEmail(email);
            if (user.length > 0) {
                util.setSuccess(200, 'Users retrieved', user);
            } else {
                util.setSuccess(200, 'No Users found');
            }
            return util.send(res);
        } catch (error) {
            util.setError(400, error);
            return util.send(res);
        }
    },

    createEmployeeInAuth: async (req, res) => {
        const newEmployee = req.body;
        try {
            const createdEmployee = await Auth0Service.createEmployeeInAuth(newEmployee);
            util.setSuccess(201, 'Employee Added!', createdEmployee);
            return util.send(res);
        } catch (error) {
            util.setError(400, error.message);
            return util.send(res);
        }
    },
    updateAuth0Employee: async (req, res) => {
        const alteredPassword = req.body;
        const { email } = req.params;
        const password = {"password":2};
        try {
            const id = await Auth0Service.getAuthUserByEmail(email);
            const employeeId = await EmployeeService.getEmployeeByEmail(email);
            const updateEmployee = await EmployeeService.updateEmployee(employeeId.id, password);
            console.log(updateEmployee);
            const updateAuth0Employee = await Auth0Service.updateEmployee(id, alteredPassword);
            if (!updateAuth0Employee) {
                util.setError(404, `Cannot find a Client with the id: ${id}`);
            } else {
                util.setSuccess(200, 'Client updated', updateAuth0Employee);
            }
            return util.send(res);
        } catch (error) {
            util.setError(404, error);
            return util.send(res);
        }
    },

    deleteEmployeeInAuth: async (req, res) => {
        try {
            await Auth0Service.deleteEmployeeInAuth(employeeId);
            util.setSuccess(201,"employee from auth0 deleted",employeeId)
            return util.send(res);
        } catch (error) {
            return error;
        }
    }
}

module.exports = EmployeeController;