const sequelize = require('../../config/sequelize-config').sequelize
const DataTypes = require('../../config/sequelize-config').datatype

const Company = sequelize.define('Company', {
    id:{
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    nombre: {
        type: DataTypes.STRING,
        allowNull: false
    }
});
Company.associate = function(models){
    Company.hasMany(models.Employee, {as: 'employees'});
    Company.hasMany(models.Client, { as: 'clients' });
  };
module.exports = Company;