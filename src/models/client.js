const sequelize = require('../../config/sequelize-config').sequelize
const DataTypes = require('../../config/sequelize-config').datatype

const Client = sequelize.define('Client', {
    id:{
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    nombre: {
        type: DataTypes.STRING,
        allowNull: false
    },
    numero: {
        type: DataTypes.INTEGER,
    },
    codigoContrato: {
        type: DataTypes.STRING,
    },
    direccion: {
        type: DataTypes.STRING,
    },
    detalle: {
        type: DataTypes.STRING,
    },
    equipoFrio: {
        type: DataTypes.STRING,
    },
    descripcionEquipoFrio: {
        type: DataTypes.STRING,
    },
    celularEquipoFrio: {
        type: DataTypes.STRING,
    },
    CompanyId: {
        type: DataTypes.STRING,
    },
});
Client.associate = function(models){
    Client.hasMany(models.Schedule, {as: 'schedules'})
    Client.belongsTo(models.Company, {foreignKey: 'companyId', as: 'company'});
  };
module.exports = Client;