const sequelize = require('../../config/sequelize-config').sequelize
const DataTypes = require('../../config/sequelize-config').datatype

const Schedule = sequelize.define('Schedule', {
    id:{
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    minuto: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    hora: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    dia: {
        type: DataTypes.STRING,
        allowNull: false
    },
    ClientId: {
        type: DataTypes.INTEGER,
    },
});
Schedule.associate = function(models){
    Schedule.belongsTo(models.Client, {foreignKey: 'ClientId', as: 'client'});
  };

module.exports = Schedule;
