const sequelize = require('../../config/sequelize-config').sequelize
const DataTypes = require('../../config/sequelize-config').datatype

const Employee = sequelize.define('Employee', {
    id:{
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    nombre: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
    },
    password: {
        type: DataTypes.INTEGER,
    },
    CompanyId: {
        type: DataTypes.STRING,
    }
});

Employee.associate = function(models){
    Employee.belongsTo(models.Company, {foreignKey: 'CompanyId', as: 'company'});
  };
module.exports = Employee;